﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDataGridColumnHeaderSpan
{
    /**
     * @brief サンプルデータ
     */
    class SampleData
    {
        public string Name { get; set; }
        public string Data1 { get; set; }
        public string Data2 { get; set; }
        public string Data3 { get; set; }
        public string Data4 { get; set; }
    }

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        //! スクロール同期処理
        private DataGridScrollSynchronizer ScrollSynchronizer;

        /**
         * @brief コンストラクタ
         */
        public MainWindow()
        {
            InitializeComponent();

            // ヘッダとするデータグリッドのデータを設定します。
            // データ行が無いとスクロールしません。
            var headerList = new List<SampleData>();
            headerList.Add(new SampleData() { Name = "" });
            dataGridHeader.ItemsSource = headerList;

            // データ行となるデータグリッドにデータを設定します。
            var dataList = new List<SampleData>();
            dataList.Add(new SampleData() { Name = "名前1" });
            dataList.Add(new SampleData() { Name = "名前2" });
            dataList.Add(new SampleData() { Name = "名前3" });
            dataGridData.ItemsSource = dataList;
        }

        /**
         * @brief ウィンドウが描画された後に呼び出されます。
         * 
         * @param [in] sender ウィンドウ
         * @param [in] e イベント
         */
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            // データ行の高さを0に設定して、非表示にします。
            var row = GetDataGridRow(dataGridHeader, 0);
            row.Height = 0;

            // データグリッドのスクロール同期を設定します。
            var dataGridList = new List<DataGrid>();
            dataGridList.Add(dataGridHeader);
            dataGridList.Add(dataGridData);
            ScrollSynchronizer = new DataGridScrollSynchronizer(dataGridList, SynchronizeDirection.Horizontal);
        }

        // 内部メソッド(詳細は省略します)

        /**
         * @brief データグリッドの行オブジェクトを取得します。
         * 
         * @param [in] dataGrid データグリッド
         * @param [in] rowIndex 行番号(0起算)
         * @return データグリッドの行オブジェクト
         */
        private DataGridRow GetDataGridRow(DataGrid dataGrid, int rowIndex)
        {
            var generator = dataGridHeader.ItemContainerGenerator;
            var row = generator.ContainerFromIndex(rowIndex) as DataGridRow;
            if (row == null)
            {
                dataGridHeader.UpdateLayout();
                var item = dataGridHeader.Items[rowIndex];
                dataGridHeader.ScrollIntoView(item);
                row = generator.ContainerFromIndex(rowIndex) as DataGridRow;
            }
            return row;
        }
    }
}

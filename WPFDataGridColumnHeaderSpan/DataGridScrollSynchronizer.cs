﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPFDataGridColumnHeaderSpan
{
    /**
     * @brief スクロールの同期する方向
     */
    [Flags]
    public enum SynchronizeDirection
    {
        //! 水平方向
        Horizontal = 0x01,

        //! 垂直方向
        Vertical = 0x02,

        //! 両方
        Both = 0x03,
    }

    /**
     * @brief データグリッドスクロール同期クラス
     */
    public class DataGridScrollSynchronizer
    {
        //! スクロールビューワーリスト
        private List<ScrollViewer> ScrollViewerList;

        //! スクロール方向
        private SynchronizeDirection Direction { get; set; }

        /**
         * @brier コンストラクタ
         * 
         * @param [in] dataGridList 同期するデータグリッドリスト
         * @param [in] direction 同期するスクロール方向
         */
        public DataGridScrollSynchronizer(List<DataGrid> dataGridList, SynchronizeDirection direction = SynchronizeDirection.Both)
        {
            ScrollViewerList = new List<ScrollViewer>();

            // データグリッド数を取得する。
            int dataGridNum = dataGridList.Count;

            // 同期するデータグリッド数が1以下の場合、何もしない。
            if (dataGridNum < 2)
            {
                return;
            }

            // データグリッド数分繰り返す。
            for (int i = 0; i < dataGridNum; ++i)
            {
                // データグリッドのスクロールビューワーを取得する。
                var dataGrid = dataGridList[i];
                var scrollViewer = GetScrollViewer(dataGrid);

                // スクロールビューワーにイベントハンドラを設定する。
                scrollViewer.ScrollChanged += ScrollChanged;

                // スクロールビューワーを識別するためタグを設定する。
                scrollViewer.Tag = i;

                // スクロールビューワーリストに保存する。
                ScrollViewerList.Add(scrollViewer);
            }

            // スクロール方向を保存する。
            Direction = direction;
        }

        /**
         * @brief スクロールビューワーを取得する。
         * 
         * @param [in] element エレメント
         * @return スクロールビューワー
         *         取得できない場合、nullを返却する。
         */
        private ScrollViewer GetScrollViewer(FrameworkElement element)
        {
            // 引数elementのビジュアルオブジェクト数分繰り返す。
            var childrenNum = VisualTreeHelper.GetChildrenCount(element);
            for (int i = 0; i < childrenNum; ++i)
            {
                // ビジュアルオブジェクトを取得する。
                var child = VisualTreeHelper.GetChild(element, i) as FrameworkElement;

                // ビジュアルオブジェクトが取得できない場合
                if (child == null)
                {
                    // 次を取得する。
                    continue;
                }

                // 取得したビジュアルオブジェクトがスクロールビューワーの場合
                if (child is ScrollViewer)
                {
                    // 取得したスクロールビューワーを返却する。
                    return child as ScrollViewer;
                }

                // 次のビジュアルオブジェクトを取得する。
                child = GetScrollViewer(child);
                if (child != null)
                {
                    return child as ScrollViewer;
                }
            }
            return null;
        }

        /**
         * @brief スクロールされた時に呼び出される。
         * 
         * @param [in] sender スクロールビューワー
         * @param [in] e スクロールチェンジイベント
         */
        private void ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var srcScrollViewer = sender as ScrollViewer;

            // 同期するスクロール方向が水平方向の場合
            if (Direction.HasFlag(SynchronizeDirection.Horizontal))
            {
                // スクロールするオフセットを取得する。
                var offset = srcScrollViewer.HorizontalOffset;

                // スクロールビューワー数分繰り返す。
                foreach (var dstScrollVierwer in ScrollViewerList)
                {
                    // スクロールしたスクロールビューワーは無視する。
                    if (dstScrollVierwer.Tag == srcScrollViewer.Tag)
                    {
                        continue;
                    }

                    // 同期するスクロールビューワーをスクロールする。
                    dstScrollVierwer.ScrollToHorizontalOffset(offset);
                }
            }

            // 同期するスクロール方向が垂直方向の場合
            if (Direction.HasFlag(SynchronizeDirection.Vertical))
            {
                // スクロールするオフセットを取得する。
                var offset = srcScrollViewer.VerticalOffset;

                // スクロールビューワー数分繰り返す。
                foreach (var dstScrollVierwer in ScrollViewerList)
                {
                    // スクロールしたスクロールビューワーは無視する。
                    if (dstScrollVierwer.Tag == srcScrollViewer.Tag)
                    {
                        continue;
                    }

                    // 同期するスクロールビューワーをスクロールする。
                    dstScrollVierwer.ScrollToVerticalOffset(offset);
                }
            }
        }
    }
}
